#!/usr/bin/env python
from livereload import Server, shell
server = Server()
server.watch('../charts/**', shell('make live-commit'))
server.watch( '../apps/**', shell('make live-commit'))
server.serve(port=8002) 
